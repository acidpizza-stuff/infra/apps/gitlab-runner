# Gitlab Runner

[[_TOC_]]

## Usage

```bash
# View all commands
make

# install docker runner
make start

# register to gitlab
make register

# configure (if necessary)
make config
```


## Prometheus Monitoring

Add this configuration in `/etc/gitlab-runner/config.toml`.

```toml
listen_address = ":9252" # Export prometheus metrics
```


## Use Cases

### Mimic shell executor using SSH executor

Use `make config` to edit the runner config. `172.17.0.1` is the Linux host IP from the container.

```toml
[[runners]]
  executor = "ssh"
  [runners.ssh]
    host = "172.17.0.1"
    port = "22"
    user = "deploybot"
    identity_file = "/home/gitlab-runner/id_ed25519"
    disable_strict_host_key_checking = true
```

Create SSH identity with the following steps. The login user [needs to have .bash_logout removed](https://docs.gitlab.com/runner/faq/#job-failed-system-failure-preparing-environment) to prevent error in preparing environment.

A `builds` folder will be created in the login's user's home folder to hold the gitlab runner builds.

```bash
# Create user
useradd -m -s /bin/bash deploybot
rm /home/deploybot/.bash_logout

# Allow user to access docker
usermod -aG docker deploybot

# Create SSH folder
su - deploybot
mkdir -p ~/.ssh
chmod -R 700 ~/.ssh

# Create keypair for user
ssh-keygen -q -N "" -t ed25519 -f /home/deploybot/.ssh/id_ed25519

# Add identity file to gitlab runner container
docker cp /home/deploybot/.ssh/id_ed25519 gitlab-runner:/home/gitlab-runner
docker exec -it gitlab-runner /bin/bash
chown gitlab-runner:gitlab-runner /home/gitlab-runner/id_ed25519
exit

# Add authorized key
ssh-copy-id -i /home/deploybot/.ssh/id_ed25519.pub deploybot@127.0.0.1
# Alternatively
cat /home/deploybot/.ssh/id_ed25519.pub >> /home/deploybot/.ssh/authorized_keys
chmod 644 /home/deploybot/.ssh/authorized_keys
chown deploybot:deploybot /home/deploybot/.ssh/authorized_keys
```

### Docker-in-Docker (DinD)

Reference: https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#docker-in-docker-with-tls-enabled-in-the-docker-executor

Add the following configuration to a docker executor runner.

```toml
[[runners]]
  url = "https://gitlab.com/"
  token = TOKEN
  executor = "docker"
  [runners.docker]
    tls_verify = false
    image = "docker:24.0.5"
    privileged = true                      # set privileged = true
    disable_cache = false
    volumes = ["/certs/client", "/cache"]  # added "/certs/client"
```

Use the following configuration in `.gitlab-ci.yml`.

```yaml
default:
  services:
  - docker:24.0.5-dind

variables:
  DOCKER_TLS_CERTDIR: "/certs"

# Use docker
job1:
  image: docker:24.0.5
  script:
  - docker build -t my-docker-image .
  - docker run my-docker-image /script/to/run/tests
```


## Archived Use Cases

### Use shell executor

This only gives access to the gitlab runner container.

Use `make start-shell-docker` to add `docker` capability to the shell runners.

Add a `docker-compose.override.yml` file to extend functionality.

[Lists are appended](https://docs.docker.com/compose/compose-file/13-merge/#sequence) instead of replaced by default.

```yaml
# Add access to additional folders from shell runner
services:
  runner:
    volumes:
    - "/path/to/folder/:/path/to/folder/"
```

**Issues**: 
- Need to build a custom runner
- Need to create `docker-compose.override.yml` file to add custom mount paths
- Job will run as user `gitlab-runner` in the gitlab runner container. This will face permission issues writing to the externally mounted folders.
- Insecure to implement a container breakout

### Mimic shell executor using docker executor

Use `make config` to edit the runner config.

The image `custom-deployer` needs the tools required for deployment (eg. `docker`, `Make`, and `git`). We can build this locally with `make create-deployer-img`.

Add volume bind mounts to give access to the deployment folder and the docker socket.

```toml
[[runners]]
  [runners.docker]
    image = "custom-deployer"
    pull_policy = "if-not-present"
    volumes = ["/path/to/deployment:/path/to/deployment", "/var/run/docker.sock:/var/run/docker.sock", "/cache"]
```

**Issues**: 
- Need to build a custom image with required tools
- Need to specify custom mount paths in the runner config
- Job will run as user `root` in the docker container. This will face permission issues writing to the externally mounted folders.
