# -------------------------------------------------------------------------
# Ensure Makefile recognizes bash syntax rather than the default sh shell
SHELL := /bin/bash
# -------------------------------------------------------------------------
# Automatically print all commands with help description denoted after ## in same line
.DEFAULT_GOAL := help
.PHONY: help
help: ## Help command
	@awk 'BEGIN {FS = ":.*##"; \
	printf "\033[33mUsage:\033[0m\n  make <target>\n\n", "" } \
	/^[a-z0-9A-Z_-]+:.*?##/ { printf "  \033[32m%-20s\033[0m %s\n", $$1, $$2 } \
	/^##/ { printf "\n\033[33m%s\033[0m\n", substr($$0, 4) } ' $(MAKEFILE_LIST)

## Standard Runner

.PHONY: pull
pull: ## Update images
	@docker compose --project-name gitlab-runner pull

.PHONY: start
start: ## Start runner
	@docker compose --project-name gitlab-runner up -d --remove-orphans --wait

.PHONY: stop
stop: ## Stop runner
	@docker compose --project-name gitlab-runner down -t 30 --remove-orphans

.PHONY: restart
restart: stop start ## Restart runner

## Administration

.PHONY: config
config: ## Configure runner
	@docker run -it --rm -v gitlab-runner_gitlab-runner-config:/config -w /config bash

.PHONY: data
data: ## View runner data
	@docker run -it --rm -v gitlab-runner_gitlab-runner-home:/data -w /data bash

.PHONY: exec
exec: ## Exec into runner
	@docker exec -it gitlab-runner /bin/bash


## Register Runner

.PHONY: register-general
register-general: ## Register runner
	@docker exec -it gitlab-runner gitlab-runner register

ifndef dockerimage
override dockerimage = "bash:5.2.21"
endif
.PHONY: register-docker
register-docker: ## Register docker runner
	@if [ -z $(token) ]; then echo "error: authentication token is not set. Rerun with token=<authentication_token>. Can also override dockerimage." && exit 1; fi;\
	docker exec -it gitlab-runner gitlab-runner register -n \
		--url https://gitlab.com \
		--token $(token) \
		--executor docker \
		--description "docker-runner" \
		--docker-image "$(dockerimage)"

.PHONY: register-dind
register-dind: ## Register dind runner
	@if [ -z $(token) ]; then echo "error: authentication token is not set. Rerun with token=<authentication_token>" && exit 1; fi;\
	docker exec -it gitlab-runner gitlab-runner register -n \
		--url https://gitlab.com \
		--token $(token) \
		--executor docker \
		--description "dind-docker-runner" \
		--docker-image "docker:24.0.5" \
		--docker-privileged \
		--docker-volumes "/certs/client"

ifndef sshuser
override sshuser = "deploybot"
endif
ifndef sshhost
override sshhost = "172.17.0.1"
endif
.PHONY: register-ssh
register-ssh: ## Register SSH runner
	@if [ -z $(token) ]; then echo "error: authentication token is not set. Rerun with token=<authentication_token>. Can also override sshuser, sshhost." && exit 1; fi;\
	docker exec -it gitlab-runner gitlab-runner register -n \
		--url https://gitlab.com \
		--token $(token) \
		--executor ssh \
		--description "ssh-runner" \
		--ssh-host "$(sshhost)" \
		--ssh-port "22" \
		--ssh-user "$(sshuser)" \
		--ssh-identity-file "/home/gitlab-runner/id_ed25519" \
		--ssh-disable-strict-host-key-checking "true"
